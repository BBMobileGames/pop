﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace POP
{
    public class TCPClient
    {
        public Thread t;
        public Socket ClientSocket;
        public TCPClient()
        {
            t = new Thread(new ThreadStart(ClientThreadStart));
            t.Start();
        }

        private void ClientThreadStart()
        {
            ClientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            ClientSocket.Connect(new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8888));
            Console.WriteLine("Connected to localhost.");

            // Receive the data whenver available.
            while (true)
            {
                ClientSocket.Send(Encoding.ASCII.GetBytes("Hello"));
                Console.WriteLine("Data send -- Hello");
                Thread.Sleep(20000);
            }
            ClientSocket.Close();
        }
    }
}
